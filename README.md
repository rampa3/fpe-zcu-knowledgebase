# FPE ZČU Knowledgebase

## O co jde?

Tento repozitář obsahuje v Markdownu psané poznámky k různým předmětům na různých oborech FPE ZČU.

## Jak s repozitářem pracovat

Poznámky jsou zpracovány v programu [Zettlr](https://www.zettlr.com/) (zdarma, open source), který je také vhodný k jejich prohlížení. Aktuálně jediná jeho chyba je, že některé způsoby formátování textu zobrazuje jen jako jejich tagy (konkrétně aktuálně pro tento repozitář jde o superscripty a subscripty, které se v Zettlru ukazují jen jako jejich tagy - `<sup></sup>` a `<sub></sub>`), na rozdíl od zobrazení v repozitáři přímo na GitLabu, kde se text formátuje korektně. Pokud by jste chtěli cokoliv přispět, použijte prosím taktéž již zmíněný editor [Zettlr](https://www.zettlr.com/). Pro editační přístup požádejte @rampa3, aby byl Váš účet GitLab přidán do účtů s právy editace.
