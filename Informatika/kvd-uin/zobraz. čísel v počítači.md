#zobrazení, #IEEE-754
# Zobrazení čísel v počítači
- v počítači operujeme s binárními čísly
- problémy:
    - nutno znát počet zobrazených míst
    - nutnost rozhodnout jak bude naloženo se znaménkem
## Přímý kód
- jedna z forem zobrazení celých čísel v počítači
- první pozice = znaménko, ostatní pozice - převod postupným dělením -> výsledek je tedy soupis zbytků postupného dělení (převod do dvojkové soustavy) + číslice značící znaménko, připsána k výsledku převodu z leva (+ před ni pak nuly pro doplnění počtu míst)
- příklad:
    - převod na $PK$
        | $0$ |  $0111111_{2PK}$ |
        | - | - |
        | Znaménko | Převod čísla $63_{10}$ |
        | $63_{10}$ je kladné -> $0$ | $63_{10} = 111111_{2}$ |
        
        $63 = 31 * 2 + 1$
        $31 = 15 * 2 + 1$
        $15 =  7 * 2 + 1$
         $7 =  3 * 2 + 1$
         $3 =  1 * 2 + 1$
         $1 =  0 * 2 + 1$
        
        Výsledek: $63_{10} = 00111111_{2PK}$
   
    - převod z PK
        | $1$ | $0101010_{2PK}$ |
        | - | - |
        | Znaménko | Převod zbytku $PK$ do desítkové soustavy |
        | $1$ -> $-$ | $(0)101010_{2}$ -> $42$ |
        
        $H_{10} = 1 * 2^{5} + 1 * 2^{3} + 1 * 2^{1}$
        
        Výsledek: $10101010_{2PK} =  − 42_{10}$
        
- ošemetnost $PK$ - existují $2$ nuly
    - příklad:
        na osmi místech: $10000000_{2PK}$ a $00000000_{2PK}$
## Dvojkový doplněk
- pro kladná čísla stejný jako $PK$
- pro záporná čísla:
    - Technický převod
        - vytvoříme $PK$ absolutní hodnoty čísla
        - provedeme inverzi tohoto $PK$
        - přičteme $1_{2}$
    - Převod přes kapacitu
        - přičteme zobrazované záporné číslo ke kapacitě dvojkové soustavy o zadaném počtu míst $m$
        - získané číslo převedme jako celek do dvojkové soustavy jedním ze dříve zmíněných způsobů
- převod z $DD$ do desítkové soustavy:
    - pro kladná čísla provedeme převod z dvojkové do desítkové soustavy
    - pro záporná čísla:
        - nejprve provedeme převod s využitím kapacity dvojkové soustavy na počtu míst rovnající se počtu cifer v čísle
        - od výsledku převodu v desítkové soustavě odečteme $2$ umocněno na počet cifer původního $DD$
## IEEE 754
- specifikuje formát reprezentace čísel v tzv. normalizovaném tvaru
- oštření zvláštních případů:
    - podtečení/přetečení - denormalizovaná/± nekonečno
        - nekonečna:
            | $∞$  | $-∞$ |
            | - | - |
            | $s = 0$ | $s = 1$ |
            | $e =$ samé $1$ | $e =$ samé $1$ |
            | $f =$ samé $0$ | $f =$ samé $0$ |
            
            kde $s =$ signum (znaménko), $e =$ exponent a $f =$ fraction (desetinná část mantisy)
    - nula
        - nuly:
            | $+0$  | $-0$ |
            | - | - |
            | $s = 0$ | $s = 1$ |
            | $e =$ samé $0$  | $e =$ samé $0$ |
            | $f =$ samé $0$ | $f =$ samé $0$ |
            
            kde $s =$ signum (znaménko), $e =$ exponent a $f =$ fraction (desetinná část mantisy)
    - Not a Number: $s =$ 0 nebo 1, $e =$ samé $1$ a $f ≠ 0$
- $2$ formáty
    - single: $s = 1$, $e = 8$ a $f = 23$ (bias = $127, 32$bit)
        - výpočet: $(-1)^{s} * m * 2^{e}$
        - příklad výpočtu:
            - single $= (-1)^{s} * m * 2^{e} = 16,25$,
                kde
                | Značení | Název | Význam |
                | - | - | - |
                | $s$ | signum | $0/1 = +/-$ |
                | $e$ | exponent | $e + 127$ |
                | $m$ | mantisa | $H_{10}/2^{e}$; mantisa se nachází na intervalu $(1;2)$ |
                | $f$ | fraction | desetinná část mantisy |
            - $m = 16,25/2^{4} = 1,015625$
            - převod postupným dělením:
                $0,015625_{10} -> 0,00101_{2}$
                $0,015625 * 2 = 0 + 0,3125$
                $0,3125 * 2 = 0 + 0,625$
                $0,625 * 2 = 1+ 0,25$
                $0,25 * 2 = 0 + 0,5$
                $0,5 * 2 = 1 + 0$
            - $e = 4_{10} + 127 = 131_{10} = 1 000 0011_{2}$
            - $H_{10} -> H_{2}: s_{2} - e_{2} - f_{2} - d$
                ($d$ = doplnění do bitů)
            - | $s$ | $e$ | $f$ | $d$ |
                | — | — | — | — |
                | $0$ | $100 001 1$ | $000 01$ | ($0000$) $* 4$ |
            - $H_{2}$ -> $H_{10}: s = +/-; e_{2}$ -> $e_{10} - 127$; $f$ = zleva doprava, záporné mocniny
    - double:  $s = 1$, $e = 11$ a $f = 52$ (bias $= 1023, 64$bit)
- Zobrazní čísla v IEEE 754
    -  nejprve zapíšeme číslo v semilogaritmickém tvaru
    -  exponent pak zobrazíme v kódu s posunutou nulou přičtením čísla $127_{10}$
    -  a zlomkovou část mantisy opět převedeme do dvojkové soustavy běžným způsobem
- Získání čísla zobrazeného v IEEE 754
    - znaménko ($s$) získáme dle hodnoty na nejvýznamnějším místě
    - exponent ($e$) z následujících pozic získáme tím, že takto získané číslo převedeme do desítkové soustavy a odečteme od něj $127_{10}$ (exponent byl v kódu s posunutou nulou)
    - desetinnou část mantisy převedeme běžným způsobem do desítkové soustavy - získáme hodnotu $f$
    - převod dokončíme dosazením do vztahu $H_{10} = (-1)^{s} * (1 + f) * 2^{e}$
## Shrnutí
- je třeba vždy vědět na jaký počet míst zobrazení probíhá - ovlivňuje zobrazitelný rozsah
- $PK$ nevhodný pro HW implementaci - složité sčítání, může přetéct