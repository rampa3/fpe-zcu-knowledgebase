#číselné-soustavy, #převod
# Číselné soustavy a převody mezi nimi
- číselná soustava = obecně způsob zobrazování čísel
- číselné soustavy dělíme na nepoziční (nepolyadické) a poziční (polyadické)
    - poziční/polyadické - pozice cifry udává její řád
    - nepoziční/nepolyadické - hodnota cifry dána bez ohledu na pozici v čísle

## Nepoziční soustavy
- např. římská čísla
- nejsou široce používány - jejich zápis je neprakticky dlouhý

## Poziční soustavy
- číslo představováno součtem mocninné řady:

$H = a_{n-1} * Z^{n-1} + … + a_{0} * Z^{0} + … + a_{-m} * Z^{-m}$,

kde každý sčítanec je hodnotou pozice \* cifrou na pozici

| Značení | smysl |
|--------------|----------------|
| $a_{1}$ | cifra od $0$ do $Z-1$ (příp. znak), kterou lze v soustavě využít |
| $Z$ | základ (báze) soustavy |
| $n$ | nejvyšší řád s nenulovou cifrou |
| $m$ | nejnižší řád s nenulovou cifrou |

- zápis čísla tvořen celou a zlomkovou částí; části jsou odděleny řádovou čárkou
- příklady:

| Soustava | Rozpis čísla |
|--------------|----------------|
| $10$ | $5 *10^{1} + 4 * 10^{0} + 3 * 10^{-1} = 54,3_{10}$ |
| $2$ | $1 * 2^{2} + 0 * 2^{1} + 1 * 2^{0} + 0 * 2^{-1} + 1 * 2^{-2} = 101,01_{2}$ |
| $16$ | $10 * 16^{1} + 8 * 16^{0} + 15 * 16^{-1} + 4 * 16^{-2} = A8F4_{16}$ |
| $3$ | $2 * 3^{2} + 1 * 3^{1} + 0 * 3^{0} + 1 * 3^{-1} + 2 * 3^{-2} = 210,12_{3}$ |
| $8$ | $7 * 8^{3} + 5 * 8^{2} + 3 * 8^{1} + 1 * 8^{0} + 0 * 8^{-1} + 2 * 8^{-2} + 4 * 8^{-3} + 4 * 8^{-4} = 7531,0244_{8}$ |

### Desítková (dekadická) soustava
- běžně nejpoužívanější

### Dvojková (binární) soustava
- používá pouze číslic $0$ a $1$
- pozitivní logika - $0$ false, $1$ true
- uplatňuje se v elektronických spínacích obvodech

### Osmičková (oktalová) soustava
- dříve využívána u starších procesorů
- dnes má omezené využití
- příklad dnešního využití - definice přístupových práv souborů pod \*nix systémy

### Šestnáctková (hexadecimální) soustava
- normální čísla od $0$ do $9$, $10$ až $15$ reprezentovány znaky $A$ až $F$
- využívána pro popis dat na adresové a datové sběrnici (značí se $h$ nebo $H$ za číslem), nebo také popis RGB hodnoty barvy (značení \#<číslo> - například \#FFFF00 = žlutá )

### Převody
#### Z desítkové do X
##### Celé části
###### Postupným dělením
- převáděné číslo dělíme bází cílové soustavy, a zapisujeme zbytky po dělení
    $H_{10} = k_{0} * Z + zb_{0}$
    $k_{0} = k_{1} * Z + zb_{1}$
    $k_{1} = k_{2} * Z + zb_{2}$
    $\vdots$
    $k_{n - 3} = k_{n - 2} * Z + zb_{n - 2}$
    $k_{n - 2} = 0 * Z + zb_{n - 1}$
- po dokončení dělení dostáváme výsledek zapsáním zbytků jako cifer v opačném pořadí - od posledního kroku k počátku dělení
    $H_{Z} = (zb_{n - 1} … zb_{2} zb_{1} zb_{0})_{Z}$
###### Postupným odčítáním
- nejprve určíme počet pozic čísla po převodu pomocí určení největší mocniny báze soustavy $Z$, která je ještě menší nbo rovna převáděnému číslu $H_{10}$:
    $Z^n > H_{10} \geq Z^{n-1}$
- následně určíme cifry/znaky převedeného čísla jako koeficienty počtu opakování mocniny $Z$ pro danou pozici - začneme od nejvyšší, a odečítáme dokud je zbytek větší než báze soustavy $Z$ - koeficient pozice pak je počet provedených odečtů
    $H_{10} = k_{n - 1} * Z^{n - 1} + zb_{n - 1}$
    $zb_{n - 1} = k_{n - 2} * Z^{n - 2} + zb_{n - 2}$
    $zb_{n - 2} = k_{n - 3} * Z^{n - 3} + zb_{n - 3}$
    $\vdots$
    $zb_{2} = k_{1} * Z^{1} + zb_{1}$
    $zb_{1} = k_{0} * Z^{0} + 0$
- u zbytku opakujeme tentýž postup s o řád nižší mocninou
- výsledek ískáme prostým přepisem koeficientů $k$
    $H_{Z} = (k_{n - 1} k_{n - 2} k_{n - 3} ... k_{1} k_{0})_{Z}$
##### Zlomkové části
###### Postupným násobením
- nejprve vynásobíme desetinnou část čísla z desítkové soustavy cifrou, představující základ (bázi) soustavy $Z$, do které převádíme
- na příslušnou pozici napíšeme cifru (znak), která se objevila po násobení před desetinnou čárkou
- pokud je třeba, odečtem zařídíme aby před desetinnou čárkou zůstala nula
    $H_{10} * Z = cc_{1} + zc_{1}$
    $zc_{1} * Z = cc_{2} + zc_{2}$
    $zc_{2} * Z = cc_{3} + zc_{3}$
    $...$
- číslo v nové soustavě o základu $Z$ získáme prostým přepisem celých částí
    $H_{Z} = (0,cc_{1}cc_{2}cc_{3}...)_{Z}$
###### Postupným odčítáním
- podobně jako pro celou část
#### Z X do desítkové
- vypočteme násobky pro jednotlivé pozice, počínajíc pozicí 0 (z prava) - na každé pozici násobíme bázi zdrojové soustavy umocněnou na číslo pozice cifrou na pozici
- výsledek je součtem násobků na všech pozicích původního čísla
    $H_{10} = a_{n - 1} * Z^{n - 1} + a_{n - 2} * Z^{n - 2} + … + a_{0} * Z^{0} + a_{- 1} * Z^{- 1} + … + a_{- m} * Z^{- m}$