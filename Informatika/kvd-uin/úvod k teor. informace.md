#informace
# Úvod k teorii informace
- přenos informace je základem komunikace
- přenáší se různými způsoby, převážně smysly a řečí
    - způsoby přenosu informace smysly: verbálně v textové i zvukové podobě, vizuálně, vůní, hmatem, auditivně, chuťí
- typy: vědeckotechnické, ekonomické, normotvorné, sociální, politické, ...
- problém s definicí: z latiny "uvádět ve tvar"; poznatek, údaj, novinka; energetická veličina; proces přizpůsobení se nahodilostem; obsah komunikace; schopnost uspořádávat
- abstraktní pojem, nehmotný charakter
- může být různě uložená
- lze ji přenést různými způsoby, ale informace je na použitém druhu přenosu nezávislá
- dispozice odstraňující nejistotu u příjemce
- pojmy:
    - signál - fyzikální veličina nesoucí informaci
    - abeceda - množina elementárních signálů; prvky pro tvorbu symbolů
    - zpráva - časově ohraničená posloupnost symbolů, mající ucelenou formu
    - data - stav objektu; veličina
- zpráva
    - představuje údaj či data, která pro někoho při správném vyhodnocení budou představovat informaci
    - každá zpráva má:
        - syntaxi - kvantitativní míra informace
        - sémantiku - kvalitativní pohled na informace
        - důležitost - význam zprávy pro příjemce
- vyhodnocením souboru zpráv získáme informaci
- vlastnosti informace
    - význam v čase - nová/stará
    - jedinečnost obsahu - originální/stará
    - použití - potenciální/aktuální
    - vztah k systému - vnitřní/vnější
    - prezentace - nekondenzovaná/kondenzovaná
- teorie informace
    - Claude Elwood Shannon (1916–2001) - v roce 1948 spolupublikoval článek „A mathematical theory of communication“
        - závislost pravděpodobnosti a množství informace získané přenesením zprávy o jediném symbolu $x$, který má pravděpodobnost výskytu $p(x)$ ve zprávách zdroje abecedy $X$:
            $I(x) = -\log_{2}p(x)$
        - v článku byla pro míru informace zavedena také důležitá zkratka bit (z binary digit – dvojková číslice)
        - zároveň Shannon zavedl vztah, kterým provedl zprůměrování množství informace, kterou obdržíme s jedním symbolem, přes více zpráv (či ideálně všechny zprávy, které může zdroj produkovat) - entropii pravděpodobnostního rozložení zdroje:
            $H(X) = - \displaystyle\sum_{i = 1}^{n} p(x_{i}) * \log_{2}p(x_{i})$