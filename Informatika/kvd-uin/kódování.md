#kódování, #znak
# Kódování
- i přes použité matematické vzorce s pravděpodobnostmi, nám stále v první řadě jde o komunikaci
- důvody kódování
    - přizpůsobení - přzpůsobení přenášených zpráv abecedě kanálu
    - zabezpečení - ochrana dat proti rušení
    - komprimování - lepší hospodaření s pamětí
    - šifrování - skrytí obsahu nepovolaným
- přenosový kanál - předání informace mezi zdrojem a příjemcem
    - zdrojová data kódována do tvaru přzpůsobeného vlastnostem kanálu
    - zdroje problémů při přenosu - zkreslení, šumy, útlum
    - po přijetí je třeba provést zpětnou transformaci
    - informace i signál mohou být spojité a nespojité - převod kontinuální veličiny na diskrétní - pomocí vzorkování, kvantování, kódování
    - typy podle počtu symbolů
        | Typ | Význam typu |
        | - | - | 
        | spojitý | pracuje s nekonečným počtem symbolů |
        | diskrétní | pracuje s konečným počtem symbolů |
    - typy podle času na výstupu
        | Typ | Význam typu |
        | - | - | 
        | spojitý | časový okamžik, kdy se na výstupu kanálu se objeví symbol není určen |
        | diskrétní | na výstupu kanálu se objeví symbol v určitém časovém okamžiku |
    - problémy - šum, porucha
- kódování = zobrazení mezi množinami $A$ a $B$
        $K: A \rightarrow B^*$
        , kde:
        | Označení | Význam |
        | - | - |
        | $A$ | zdrojová abeceda (tvořená zdrojovými znaky) |
        | $B$ | kódová (přenosová) abeceda (tvořená kódovými znaky) |
        | $B^*$ | kód (množina všech slov tvořených konečnou neprázdnou posloupností kódových znaků) |
     - podle počtu kódových znaků dělíme kódování na:
         | Typ | Počet kódových znaků |
         | - | - |
         | binární | dva ($0$ a $1$) |
         | ternární | tři |
         | ... | ... |            
    - jednoznačná dekódovatelnost - prosté zobrazení - jestliže se liší kódová slova, liší se i znaky zdrojové abecedy, které jim odpovídají a naopak
    - délka kódu = počet slov binárního kódu, který se pohybuje v rozmezí $1 \le L \le 2^m$, kde m je počet bitů jednoho slova
    - průměrná délka kódového slova:
        $\overline{n} = \displaystyle\sum_{i=1}^N m_{i} * p_{i}$
        , kde:
        | Značení | Význam |
        | - | - |
        | $m_i$ | je délka i-tého kódového slova |
        | $p_i$ | pravděpodobnost výskytu i-tého znaku zdrojové abecedy |
    - efektivnost kódu (%):
        $\eta = \frac{- \displaystyle\sum_{i=1}^N p_i * \log_2{p_i}}{\displaystyle\sum_{i=1}^N m_i * p_i} * 100$
        , tedy podíl entropie a průměrné délky kódového slova
- dekódování - McMillanova věta
    - vychází z Kraftovy nerovnosti
    - když víme, že zdrojový znak   ai   budeme kódovat slovem délky $m_i$, pak pro kód, který je jednoznačně dékodovatelný „znak po znaku“, platí:
        $\displaystyle\sum_{i=1}^N 2^{-m_i} \le 1$
- dělení kódů:
    - dle použití: pro přenos zpráv, pro matematické operace
    - dle struktury: systematické, nesystematické
    - dle počtu kódových znaků: rovnoměrné, nerovnoměrné
- rovnoměrné (blokové) kódy - zdrojová zpráva (slovo z množiny $A^*$) je kódována znak po znaku
    - příklady:
        | Název | Popis |
        | - | - |
        | MTA2 | telegrafní kód v dálnopisech, kódová slova tvořená pěticemi $0$ a $1$ |
        | ASCII | kódová slova tvořená osmicemi $0$ a $1$ |
        | BCD (Binary Coded Decimal) | čtveřice $0$ a $1$ pro kódování dekadických cifer |
- nerovnoměrné kódy
    - znakům zdrojové abecedy se přiřazují kódová slova s různým počtem znaků
    - příklady:
        - Morseova abeceda - ternární kód
        - Efektivní kódy, též minimální
            - při konstrukci kódu vycházíme z toho, že známe pravděpodobnost výskytu znaku zdrojové abecedy a délky kódových slov chceme mít co nejmenší
                - Huffmanova metoda konstrukce:
                    | Č. kroku | Instrukce |
                    | - | - |
                    | $1$ | zdrojové znaky seřadit sestupně podle $p_i$ |
                    | $2$ | dvě nejmenší pravděpodobnosti výskytu sečíst a znovu seřadit (opakovat dokud není součet $1$) |
                    | $3$ | sčítancům zpětně postupně přidělovat kódové znaky $0$ a $1$ |
                - Shannon-Fanova konstrukce:
                    | Č. kroku | Instrukce |
                    | - | - |
                    | $1$ | zdrojové znaky seřadit sestupně podle $p_i$ |
                    | $2$ | vytvořit dvě skupiny znaků, jejichž součet pravděpodobností je podobný |
                    | $3$ | znakům v první skupině přiřadit kódový znak $1$, ve druhé $0$ |
                    | $4$ | dělení ukončit až jsou přítomny pouze jednočlenné skupiny |
                    - příklad:
                    - | Znaky | Pravděpodobnosti výskytu znaků | Kódová slova |
                        | - | - | - |
                        | $I$ | $0,35$ | $0$ |
                        | $L$ | $0,3$ | $10$ |
                        | $P$ | $0,2$ | $110$ |
                        | $F$ | $0,15$ | $111$ |
                        - $I$ = $0$; $L$ = $10$; $P$ = $110$; $F$ = $111$

    - konstrukce - grafy
- prefixové kódy - žádné kódové slovo není předponou jiného
    - platnost McMillanovy věty je pro ně podmínkou nutnou, ale nikoli postačující
    - Shannon-Fanova metoda
            1. pravděpodobnosti sestupně seřadíme
            2. pravděpodobnosti rozdělíte na dvě přibližně stejné skupiny
            3. jedné skupině přiřadíte znak $1$, druhé skupině znak $0$
            4. kroky $2$ a $3$ opakujete, dokud všechny skupiny nebudou jednoprvkové
        - příklad:
            $D = 18 \%$, $E = 36 \%$, $F = 6 \%$, $K = 10 \%$, $P = 21 \%$, $V = 9 \%$
        
            1. zdrojové znaky seřadíme podle pravděpodobností: $E$, $P$, $D$, $K$, $V$, $F$
            2. vytvoříme dvě skupiny v poměru $57:43$ ($EP:DKVF$) (poměry $E:PDKVF$ i $EPD:KVF$ by byly horší)
            3. první skupině přidělíme $1$, druhé skupině $0$
            4. skupinu $EP$ rozdělíme v poměru $36:21$ ($E:P$)
            5. zdrojovému znaku $E$ přidělíme $1$ ($11$), zdrojovému znaku $P$ přidělíte $0$ ($10$)
            6. skupinu $DKVF$ rozdělíme v poměru $18:25$ ($D:KVF$) (poměr $DK:VF$ by byl horší)
            7. zdrojovému znaku $D$ přidělíme $1$ ($01$), skupině $KVF$ přidělíte $0$
            8. skupinu $KVF$ rozdělíme v poměru $10:15$ ($K:VF$) (poměr $KV:F$ by byl horší)
            9. zdrojovému znaku $K$ přidělíme $1$ ($001$), skupině $VF$ přidělíte $0$
            10. skupinu $VF$ rozdělíme v poměru $9:6$ ($V:F$)
            11. zdrojovému znaku $V$ přidělíme $1$ ($0001$), zdrojovému znaku $F$ přidělíte $0$ ($0000$)
- kódy pro matematické instrukce
    - stálá délka kódové složky
    - známe – přímý kód, inverzní kód, dvojkový doplněk, kód s posunutou nulou - $L = 2^m$
    - další – kód $N+3$, váhové kódy, kódy se změnou v n řádech, kódy zbytkových tříd
    - váhové kódy:
        - výhodné vlastnosti při početních operacích s dvojkově vyjádřenými desítkovými čísly
        - ke každému koeficientu $a_i$ na všech řádových místech kódové složky přiřazujeme váhu $w_i$
            - příklady vah – $8421$, $84-1-2$, $2421$
            $N = \displaystyle\sum_{i=0}^{m-1} a_i * w_i$
            $a_{i}$ – kódový znak (v našem případě $0$ nebo $1$) na $i$. pozici v kódovém slově
            $w_{i}$ – hodnota $i$. váhy (číslo od $-9$ do $9$ různé od $0$) v kódovém slově
        - mezi nejznámější váhové kódy patří BCD kód (Binary Coded Decimal), který má váhy $w_{3} = 8$, $w_{2} = 4$, $w_{1} = 2$, $w_{0} = 1$ (zkráceně $8421$)
- kódy se změnou v $n$ - kódy měnící následující kódové slovo vůči předchozímu v n místech
    - $n$ je počet řádů v nichž se liší dvě sousední kódové slova $d(C_i,C_{i+1})$
    - pro $n = m$ je $L = 2$
    - $m > n$
        - pro $n$ sudé je $L = 2^{m-1}$
        - pro $n$ liché je $L = 2^m$
    - Grayův kód (patří mezi kódy se změnou v n řádech)
        - $n = 1$
        - používá se při převodu analogových signálů na digitální
        - pozice Karnaughovy mapy
# Kódování znaků
- znaková sada (CCS)
    -  zobrazení mezi množinou znaků a množinou nezáporných celých čísel
    -  např.: Unicode
- CEF (Character Encoding Form) - zápis v bitech
- charset (CES)\
    - způsob mapování kódových jednotek do posloupnosti bytů
    - např.: UTF-8
- American Standard Code for Information and Interchange (US-ASCII)
    - původně sedmibitový, osmý bit nulový
    - řídící znaky, číslice, velká a malá písmena anglické abecedy...
- ISO-8
    - rozšíření předchozího tím, že osmý bit se stal informačním
        - od 0 do 127 stejné jako ASCII, dále různé podle kódované národní abecedy
    - kódy pro české znaky
        - kód bratrů Kamenických
        - ISO 8859-2
        - Windows-1250
        - KOI-8
        - CP852
- Unicode
    - znaková sada, jejíž první verzi v roce 1991 vytvořilo Unicode Consortium
    - prvních 128 znaků jako u US-ASCII
    - kódové body se zapisují U+hex
    - nejprve 16bitové značky (BMP – Basic Multilingual Plane), posléze 32bitové (využívá se však jen 21 b)
    - charsety – UTF-8, UTF-16, UTF-16BE, UTF-16LE, UTF-32, UTF-32BE, UTF-32LE
        - UTF – Unicode (nebo UCS (Universal Multiple - Octet Charakter Set)) Transformation format - např.: Unicode
        - BE – Big Endian (vyšší řády jsou uloženy na nižší adrese)
        - LE – Little Endian
            
            F = U+0046:
            - UTF-16LE – 4600
            - UTF-32BE – 00000046