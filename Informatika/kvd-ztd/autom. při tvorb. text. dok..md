#najít, #nahradit, #přejít, #nastavení, #kopírování, #vkládání
# Automatizace při tvorbě textového dokumentu
## Najít, nahradit, přejít
- Word prohledává vždy dokument od místa, kde je kurzor, směrem dolů, po dosažení konce dokumentu se otevře dialogové okno s dotazem, zda chceme pokračovat v hledání od začátku dokumentu k místu, kde je kurzor
### Hledání textu
- tlačítko "Najít" na kartě "Domů" nebo Ctrl+F
### Nahrazení (záměna) slov v textu
- tlačítko "Nahradit" na kartě "Domů" – v dialogovém okně "Najít a nahradit" zapíšeme na kartě "Nahradit" do pole "Najít" slovo, které chcete nahradit a do pole "Nahradit čím" zapište slovo, kterým chceme nalezené slovo nahradit
### Přechod na
- v dialogovém okně "Najít a nahradit" na kartě "Přejít na" zvolíme typ cíle a parametry pro přechod k požadovanému cíli
## Nastavení textového editoru
- Word nabízí mnoho možností nastavení tak, aby si uživatel mohl aplikaci přizpůsobit svým potřebám
- jsou přístupné tlačítkem "Možnosti" na kartě "Soubor"
## Kopírování a vkládání
- kopírování – Ctrl+C nebo kontextové menu -> "Kopírovat"
- vložení – Ctrl+V nebo kontextové menu -> "Vložit"
    - můžeme také využít možností vložení v kontextovém menu nebo na kartě "Domů" v sekci "Schránka" k upřesnění vložení