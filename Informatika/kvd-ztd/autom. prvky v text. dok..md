#konce, #oddíly, #stránky, #záhlaví, #zápatí, #číslování, #titulky, #netextové, #prvky, #popisky, #záložky, #hypertextové, #křížové, #odkazy, #citace, #prameny, #obsahy, #seznamy, #rejstříky, #vysvětlivky, #poznámky, #odrážky
# Automatické prvky v textových dokumentech
## Konec oddílu, konec stránky
- občas se dostaneme do situace, kdy potřebujeme text na jedné straně ukončit dříve, než stránka končí a psát již na stránku další
- časté řešení – mnohonásobné použtí klávesy Enter – ŠPATNĚ!!! – toto řešení může v budoucnu způsobovat problémy např. při tisku
- správné řešení – konec stránky/oddílu
### Konec stránky
- vložíme přes kartu "Rozložení" (sekce "Vzhled stránky"), tlačítko "Konce"
### Konec oddílu
- jeden ze základních principů formátování ve Wordu – formátování po oddílech (jednotlivé oddíly lze v rámci jednoho dokumentu odlišně formátovat)
- dokument lze rozdělit na oddíly pomocí zalomení oddílu – lze jej vytvořit na kartě "Rozložení stránky" tlačítkem "Konce"
- rozdělení dokumentu na oddíly umožňuje v příslušných oddílech měnit následující prvky formátu
    - formát sloupců
    - zobrazení a polohu poznámky pod čarou a vysvětlivky
    - záhlaví a zápatí
    - číslování stránek
    - číslování řádků
    - velikost papíru a jeho orientaci
- Word nabízí čtyři způsoby zalomení oddílu
    - další stránka – ekvivalentní zalomení stránky a současně zalomení oddílu, lze zvolit např. začínáme-li další kapitolu na nové stránce a chceme-li odlišné formátování kapitol – velmi často používané
    - nepřetržitě – dochází-li k odlišnému formátování oddílů na téže stránce
    - lichá stránka – používá se, pokud nová kapitola má začínat na liché (pravé) stránce
    - sudá stránka – používá se zřídka, funguje obdobně, jako předchozí
- zalomení se v dokumentu zobrazuje dvojitou tečkovanou čarou napříč dokumentem
- lze jej zrušit jeho vymazáním (je však nutné si uvědomit, že jsou v něm "skryty" informace o formátu oddílu – po jeho smazání pak oddíl přebere formát od jiného oddílu (jde o stejný princip jako u zalomení odstavce))
## Záhlaví a zápatí, číslování stran
### Záhlaví (zápatí)
- oblast nad horním (pod dolním) okrajem textu, která se opakuje na každé stránce (v oddílu)
- lze vložit na kartě "Vložení" tlačítkem "Záhlaví" resp. "Zápatí" a vybráním příkazu "Upravit záhlaví"/"Upravit zápatí"
    - vyvoláme také levým dvojklikem na spodní či hodní okraj stránky
- text v záhlaví nebo zápatí lze psát i formátovat běžným způsobem
- při přepnutí do oblasti záhlaví a zápatí se automaticky zobrazí karta "Záhlaví a zápatí" – možnost nastavení číslování stránek, …
- tvorba odlišných záhlaví a zápatí – podmíněno rozdělením dokumentu na oddíly, nutno na kartě "Záhlaví a zápatí" vypnout možnost "Propojit s předchozím" (implicitně zapnuto) 
- při oboustranném tisku je vhodné zvolit odlišná záhlaví a zápatí na lichých a sudých stránkách – na kartě "Záhlaví a zápatí" zaškrtneme "Různé liché a sudé stránky", lze také nastavit v okně "Vzhled stránky", na kartě "Rozložení" v sekci "Záhlaví a zápatí" zatrhnutím zaškrtávacího pole "Různé liché a sudé"
- lze do záhlaví vložit název kapitoly – pole "StyleRef" pro styl "Nadpis 1" – "{STYLEREF "Nadpis 1"\n}"
### Číslování stránek
- Word nabízí různé nástroje pro automatické číslování stránek
- na kartě "Vložení" klepneme na příkaz "Číslo stránky" a vybereme umístění, ke každému umístění máme k dispozici několik formátů
- dokumenty mohou mít v každém oddílu odlišný formát číslování stránek
- odstranění – příkaz "Odebrat čísla stránek" na kartě "Vložení", pod tlačítkem "Číslo stránky"
    - lze také ručně smazat jedno z čísel – tím zrušíme číslování v celém aktuálním oddíle
 
## Titulky netextových prvků (popisky)
 - Word umožňuje automaticky číslovat např. obrázky, tabulky a další objekty
 - na kartě "Reference" v sekci "Titulky" klepneme na tlačítko "Vložit titulek" (odbodně funguje stisk pravého tlačítka myši na objektu a volba "Vložit titulek") 
## Záložky
-  v aplikaci MS Word fungují stejně jako záložky v knize – můžeme s nimi označit místo, které chceme později snadno najít
-  lze vložit ze sekce "Odkazy" na kartě "Vložení"
## Hypertextové a křížové odkazy
### Křížový odkaz
- umožňují napsat do dokumentu odkaz např. „podrobněji je uvedeno v kapitole 7"a potom nechat Word tento odkaz automaticky aktualizovat, změníte-li číslo kapitoly
- lze vložit skrze tlačítko "Křížový odkaz" na kartě "Odkazy" v sekci "Titulky"
### Hypertextový odkaz
- umožňuje z dokumentu odkázat na libovolné umístění na internetu nebo dokonce na jiný soubor v počítači (zde je však nutné si uvědomit, že pokud soubor přeneseme do jiného počítače, vazby na další soubory v původním počítači přestanou fungovat)
- vytvoříme stisknutím tlačítka Hypertextový odkaz (karta Vložení, sekce Odkazy)
## Citační prameny
- při psaní odborného článku je třeba citovat použitý zdroj informací – většinou tak, že se do textu umístí odkaz na příslušnou citaci ve formě čísla, které potom odpovídá pořadovému číslu v seznamu použité literatury
- tvorba nové citace – na kartě "Reference" v seznamu "Styl" vybereme normu "ISO 690 - číselná reference" nebo "ISO 690 - první prvek a datum" (pro zobrazení příslušných polí k vyplnění údajů), a následně stiskneme tlačítko "Přidaty nový pramen"
- pokud jsme již požadovaný zdroj alespoň jednou citovali, je možno citaci urychleně vložit skrze "Správce pramenů" – tlačítko "Spravovat prameny" na kartě "Reference"
- vložení seznamu použité literatury – na kartě Reference klepneme na tlačítko Bibliografie a vybereme příslušný formát seznamu použité literatury (s nadpisy "Bibliografie", "Citovaná literatura" nebo bez nadpisu)
## Obsah
- pro automatické vytvoření obsahu je vhodné použití stylů Nadpis 1, Nadpis 2 atd. v názvech kapitol, lze však použít i jiných stylů
- lze vložit příkazem "Vložit obsah..." na kartě "Odkazy" pod tlačítkem "Obsah"
- aktualizace obsahu (a polí obecně) – vyběr pole + F9/pravé tlačítko -> "Aktualizovat pole"
## Seznam netextových prvků (např. obrázků)
- jeho snadné vytvoření podmíněno přiřazením titulků
- např. u seznamu obrázků: pro jeho vložení na kartě "Reference" klepneme na tlačítko "Vložit seznam obrázků" (ve vyvolaném okně lze zvolit požadovaný formát seznamu)
## Rejstřík
 - je nejprve nutné v textu označit položky budoucího rejstříku a potom teprve rejstřík vytvořit
 - označení lze provést ručně i automaticky – v praxi je vhodné oba způsoby kombinovat
 - ruční označení – označíme text + Alt+Shift+X
     - odstranění ručního označení – je třeba v možnostech Wordu na kartě "Zobrazení" zatrhnout zaškrtávací pole Skrytý text, a pak u chybně označené položky umazat kód "{ XE ... }"
 - automatické\značení – nejprve je třeba vytvořit soubor pro automatické označování – dokument s tabulkou o dvou sloupcích, kde v prvním sloupci jsou exaktně hledané výrazy, a ve druhém jejich označení v rejstříku (pokud je shodné s hledaným textem, je druhý sloupec na řádku prázdný; v cílovém dokumentu pak při tvorbě rejstříku v dialogovém okně "Rejstřík" zvolíme možost "Automaticky", a zvolíme dříve vytvořený soubor
 - rejstřík lze vytvořit tlačítkem "Vytvořit rejstřík" na kartě "Reference" v sekci "Rejstřík"
## Vysvětlivky a poznámky pod čarou
- poznámka je obvykle umístěna na téže stránce, na které je na ní odkaz (je-li poznámek více, mohou pokračovat na další stránce), zatímco vysvětlivky jsou umístěny na konci dokumentu (případně oddílu, je-li na ně dokument rozdělen)
- vložení poznámky pod čarou – na kartě "Odkazy" klepneme na tlačítko "Vložit pozn. pod čarou"
- vložení vysvětlivky – na kartě "Odkazy" klepneme na tlačítko "Vložit vysvětlivku"
## Odrážky a číslování
- vytvoření seznamu s odrážkami – na kartě "Domů" klepneme na tlačítko "Odrážky" a vybereme typ odrážky, který chceme použít (můžeme i definovat vlastní odrážky)
- vytvoření číslovaného seznamu – na kartě "Domů" klepneme na tlačítko "Číslování", a zvolíme požadovaný formát číslování (lze také definovat vlastní formát)