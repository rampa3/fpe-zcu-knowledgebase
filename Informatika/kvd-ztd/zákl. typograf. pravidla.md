#typografie, #pravidla
# Co je to typografie?
- je to umělecko-technický obor, který se zabývá písmem, především pak jeho správným výběrem, použitím a sazbou
- vyvíjí se již celá staletí a její vývoj neustává, zejména kvůli zpracování textu na počítači
- typograficky správně napsaný text usnadní čtenáři jeho vnímání
- normy:
    - ČSN 01 6910 Úprava písemností psaných strojem nebo zpracovaných textovými editory
    - ČSN ISO 31-0  Veličiny a jednotky. Část 0: Všeobecné zásady
    - ČSN ISO 2145 Dokumentace - Číslování oddílů a pododdílů psaných dokumentů
    - ČSN ISO 8601 Datové prvky a formáty výměny - Výměna informací - Zobrazení data a času
# Netisknuté znaky
- běžně se s nimi setkáváme, ikdyž nejsou na první pohled viditelné
- lze je zobrazit/skrýt na kartě "Domů", v části "Odstavec"
- jejich kontrolou můžeme vyřešit problémy s formátováním či tiskem
## Pevná mezera
- zajistí, že se znaky nebo slova na jejích koncích nerozdělí na koncích řádků
- mezera má mezi netisknutelnými znaky značku malého kroužku, na rozdíl od běžné mezery, která se značí tečkou uprostřed výšky řádku
- příklady použití:
    - psaní předložek a spojek o délce jednoho písmena a následujících slov (a, i, o, s, v, z), např. v Plzni, k tobě, s tetou apod.
    - psaní čísel, např. 3 000, 2 000 000, 1,23
    - psaní čísel a značek (veličin), např. 50 %, 2 kg, 8 h
    - psaní kalendářního data - zde by neměl být oddělen den a měsíc, oddělení roku je přípustné
    - psaní čísla a počítaného jevu, např. 5 lidí, 100 koní, III. patro
# Dělení slov
- MS Word nabízí několik možností dělení slov - tlačítko pro tento nástroj nalezneme na kartě "Rozložení" v sekci "Vzhled stránky"
    - standardně - žádné - slovo přesunuto na další řádek při nedostatku místa
    - automaticky - při dosažení konce řádku, Word slovo automaticky rozdělí spojovníkem
    - ručně - v případě potřeby dělení jsme ditázáni, jak slovo rozdělit
# Čas a datum
## čas
- dle ČSN 01 6910 se čas zapisuje s dvojtečkou bez mezer (např. 22:30, 9:81:20)
- dle PČP lze použít mezi hodinami a minutami i tečku bez mezer, sekundy se však oddělují dvojtečkou vždy (22.30, 9.81:21)
- hodiny je možno psát jednomístně, minuty a sekundy vždy dvojmístně (7:05 nebo 7.05)
- označení jednotek za číslovkami lze užívat české i mezinárodní v těchto tvarech:
    - h, h., hod.
    - min i min.
    - s (bez tečky)
- v případě zápisu časového rozmezí používáme pomlčku bez mezer (např. 8–10 h, září–říjen, 11:30–12:00)
## datum
- psaní data má také několik platných způsobů
- píšeme-li datum vzestupně, je možné zapsat název měsíce číslem i slovem (1. května 2005, 1. 5. 2005)
- za tečkou u řadových číslovek se vždy píše mezera - mezeru nepíše pouze tehdy, kdy datum zapisujeme ve dvoumístném formátu (01.01.2018)
- v soukromé korespondenci píšeme měsíc slovně - datum neoddělujeme čárkou od místa napsání (Ve Starém Plzenec 11. září 2006)
- v obchodní konverzaci se využívá číselný zápis a místo konání se neskloňuje (Praha 4. 4. 2018, Plzeň 03.03.2009)
- pokud píšeme datum sestupně, používáme vždy dvoumístný formát, u roku potom čtyřmístný (2003-11-1) - jednotlivé údaje jsou oddělené spojovníkem bez mezer
- u rozsahu se používá pomlčka bez mezer, stejně jako u času, tedy 1989–2000, 1.–5. ledna
    - pouze u víceslovných výrazů je doporučeno mezery kolem pomlčky vkládat, aby nedošlo k nežádoucímu spojení výrazů, které přiléhají těsně na pomlčku, takže 1. ledna – 1. února 2018
# Matematické operátory
- základní matematické operátory v matematických operacích, poměry a rozměry uvádíme vždy s mezerami - např.:
    - 2 + 2 = 4
    - roztok ředěný v poměru 3 : 2
    - plocha o rozměru 3 m × 4 m
- znaménko + a −  píšeme bez mezery pouze tehdy, pokud vyjadřují kladnou nebo zápornou hodnotu čísla (+10 °C, −11)
- znaménko krát (×) píšeme bez mezery pokud vyjadřuje násobnou číslovku, tedy 20× = dvacetkrát
- dále bez mezer zapisujeme exponenty a indexy ($5^{3}$, $n_{1}$)
- klávesové zkratky ASCII pro znaky krát jsou následující:
    - × = Alt+0215
    - ∗ = Alt+8727
    - · = Alt+0183
    - . = Alt+46
    - × = AltGr + klávesa pro ) a (
# Spojovník a pomlčky
- spojovník je krátká vodorovná čárka, kterou často zaměňujeme s pomlčkou
- na (české) klávesnici se nachází vedle pravého Shiftu
- pomlčka je delší a na klávesnici ji nenajdeme – lze ji psát klávesovou zkratkou Alt+0150
- oba tyto symboly mají specifická využití:
    - spojovník spojuje výrazy, které jsou ve vztahu souřadném
    - pomlčka většinou výrazy odděluje a také se píše do rozsahu
# Vybrané znaky
## uvozovky
- používáme, potřebujeme-li uvést přímou řeč či doslova uvést text, název knihy apod.
- používají se také při označení cizích a nespisovných výrazů
- jsou vždy těsně u slova, které označují, tedy bez mezer
- v češtině užíváme nejčastěji dvojité uvozovky ve tvaru 99 66, tedy „tyto“
## výpustka
- výpustka (tři tečky) se používá v místech, kde je vynechána určitá část textu nebo kde je nedokončená věta
- vždy jeden znak - jakmile tři tečky po sobě napíšeme, textový editor Word je automaticky převede na výpustku
- za výpustku lze bez mezery přidat další interpunkční znaménko (čárku, otazník), nikdy však ne další tečku
- mezery používáme kolem výpustky stejně, jako u jiných interpunkčních znamének - pokud je výpustka použita např. ve výčtu je před ní mezera, po ní však následuje interpunkční znaménko bez mezery
- z obou stran výpustku oddělujeme mezerou pouze v případě, že zastupuje vynechání několika vět - v takovém případě je vhodné dát ji do závorky
## Psaní dalších znaků
- při psaní textu, zejména odborného, narazíme často na znak, který není na klávesnici - v  takových případech nám pomůže karta "Vložení" a ikona "Symbol" v pravé části nabídky
    - kliknutí na tlačítko Symbol otevře širokou galerii symbolů a značek, které lze vložit
# Nevhodné zalomení stránky a odstavce
- Word někdy automaticky zalomí stránku na místě, kde to není příliš vhodné, např. hned po nadpisu nebo uprostřed nějakého seznamu - existuje několik způsobů, jak toto odstranit
## Svázání řádků:
1. vybereme řádky, které chceme svázat
2. na kartě "Domů" klepneme na spouštěč dialogového okna "Odstavec"
3. otevře se dialogové okno "Odstavec"
4. přepneme se na kartu "Tok textu"
5. zatrhneme zaškrtávací pole "Svázat řádky"
6. klepneme na tlačítko "OK"
## Svázání odstavců dohromady:
1. vybereme odstavec, který chceme svázat s následujícím odstavcem
2. na kartě "Domů" klepneme na spouštěč dialogového okna "Odstavec"
3. otevře se dialogové okno "Odstavec"
4. přepneme se na kartu "Tok textu"
5. zatrhneme zaškrtávací pole "Svázat s následujícím"
6. klepneme na tlačítko "OK"
- tento postup je vhodný v případě, že chcete přesunout nadpis, který "zbyl" na konci stránky
## Vynucení začátku odstavce na nové stránce:
1. vybereme odstavec, který chceme umístit na začátek nové stránky
2. na kartě "Domů" klepneme na spouštěč dialogového okna "Odstavec"
3. otevře se dialogové okno "Odstavec"
4. přepneme se na kartu "Tok textu"
5. zatrhneme zaškrtávací pole "Vložit konec stránky před"
6. klepneme na tlačítko "OK"
## Vynucení začátku kapitol na nové stránce:
- pokud chcete, aby kapitola začínala vždy na nové stránce, můžete volbu "Vložit konec stránky před" přidat nadpisovému stylu, který ji uvozuje (např. stylu Nadpis 1)
1. na kartě "Domů" klepneme na spouštěč dialogového okna "Styly"
2. otevře se podokno "Styly"
3. klepneme na rozevírací tlačítko příslušného nadpisu (Nadpis 1) a zvolíme příkaz "Změnit..."
4. otevře se dialogové okno "Úprava stylu"
5. klepneme na tlačítko "Formát" a zvolíme příkaz "Odstavec"
6. otevře se dialogové okno "Odstavec"
7. zatrhneme zaškrtávací pole "Vložit konec stránky před"
8. obě otevřená dialogová okna potvrdíme tlačítkem "OK"
## Odstranění předchozích voleb:
1. vybereme odstavec (odstavce), které chcete změnit
2. na kartě "Domů" klepneme na spouštěč dialogového okna "Odstavec"
3. otevře se dialogové okno "Odstavec"
4. přepneme se na kartu "Tok textu"
5. zrušíme zatržení příslušných zaškrtávacích polí
6. klepneme na tlačítko "OK"
## Kontrola osamocených řádků:
- víceřádkových odstavců se může stát, že z odstavce zůstane na stránce jeden řádek
- Word tuto situaci automaticky potlačuje
    - postup, jak vypnout automatické potlačování výskytu osamocených řádků, je následující:
        1. na kartě "Domů" klepneme na spouštěč dialogového okna "Odstave"c
        2. otevře se dialogové okno "Odstavec"
        3. přepneme se na kartu "Tok textu"
        4. zrušíme zatržení zaškrtávacího pole "Kontrola osamocených řádků"
        5. klepneme na tlačítko "OK"






