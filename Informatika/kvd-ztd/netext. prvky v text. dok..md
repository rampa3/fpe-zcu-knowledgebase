#ilustrace, #tabulky, #rovnice, #symboly, #grafy, #SmartArt
# Netextové prvky v textových dokumentech
## Vkládání ilustrací (obrázků a obrazců)
- nalezneme na kartě "Vložení" v části "Ilustrace"
- lze vložit několik typů ilustrací
    - obrázky – obrázky z pevného disku počítače
    - online obrázky – umožňuje zadat klíčové slovo, podle něhož vyhledávač Bing filtruje obrázky na internetu (nalezené obrázky jsou pod licencí Creative Commons)
    - obrazce – rozličné obrazce, např. šipky, obdélníky nebo řečové bubliny
    - SmartArt – diagramy, schémata a pojmové mapy, které jsou vyvedeny v různých barevných provedení
    - graf
    - snímek obrazovky – snímek obrazovky libovolného okna, které máme v počítači otevřené/snímek celé obrazovky/výřez
### Formátování ilustrací
- po výběru vložené ilustrace (klepnutí na ni) se zobrazí karty s nástroji pro úpravu ilustrací
#### Obrázek
- pomocí madel (puntíků) kolem vybraného obrázku lze upravovat jeho velikost
    - je vhodné používat rohová madla – zachovávají poměr výšky a šířky
    - zatočená šipka – slouží k natočení obrázku
- karta "Nástroje obrázku" – obsahuje úpravu jasu, kontrastu, zaostření, rámečky, … – důležité nástroje – zalamovat text a oříznout
    - zalamovat text – k nastavení obtékání textu kolem obrázku – efektivní je např. použití možnosti Těsné, kdy lze s obrázkem pomocí levého tlačítka myši pohybovat po dokumentu a text se rozestupuje tak, aby nevznikala zbytečná prázdná místa
    - oříznout – umožňuje vybrat z obrázku důležitou část, kterou potřebujeme v dokumentu vidět, zbytek se odstraní
#### Obrazec
- chová se podobně jako obrázek
- karta "Nástroje kreslení – Formát" – umožňuje vybarvit obrazec, nastavit barvu jeho obrysu či např. přidat obrazci stín
- praktické obrazce – šipky nebo řečové bubliny, které pomohou popsat obrázek vložený do dokumentu
## Vkládání tabulky
- vložíme z karty "Vložení" skupina "Tabulky"
- při vybrané tabulce se objeví karty "Návrh tabulky" a "Rozložení"
### Úprava tabulky
- výška řádků a šířka sloupců – myší, přetahováním ohraničení mezi buňkami
- na kartě "Návrh tabulky" – styly tabulky a další možnosti formátování vzhledu tabulky, kresba/úprava ohraničení (tloušťka a styl)
- karta "Rozložení" – přidávání/odebírání řádků/sloupců, clučování/rozdělování buněk, zarovnání, práce s daty, …
## Vkládání rovnic a symbolů
### Symbol
- tlačítko "Symbol" na kartě Vložení
- možnost "Další symboly" v nabídce pokud není požadovaný symbol v posledně použitých/přednastavených
### Rovnice
- tlačítko "Rovnice" na kartě "Vložení"
- po sticku se vloží prázdná rovnice, a zobrazí se karta "Rovnice"
## Vkládání grafů
- tlačítko "Graf" na kartě "Vložení" (více typů na výběr)
## SmartArt
- tlačítko "SmartArt" v oblasti "Ilustrace" na kartě "Vložit"
- dá se s ním pracovat jako s jakoukoliv jinou ilustrací
- karta "Návrh SmartArtu" – vkládání dalších polí, odebírání polí, změna jejich úrovně, změna barevného schematu, …
- karta "Formát" – úprava vzhledu jednotlivých polí 